package com.skosc.tkfhw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.material.chip.Chip

class MainActivity : AppCompatActivity() {

    private val topStations = listOf(
            Station.makeRedLine("Улица Подбельского"),
            Station.makeRedLine("Черкизовская"),
            Station.makeRedLine("Преображенская площадь"),
            Station.makeRedLine("Сокольники"),
            Station.makeRedLine("Красносельская"),
            Station.makeRedLine("Комсомольская"),
            Station.makeRedLine("Красные ворота"),
            Station.makeRedLine("Чистые пруды"),
            Station.makeRedLine("Лубянка")
    )

    private val bottomStations = listOf(
            Station.makeBlueLine("Кунцевская"),
            Station.makeBlueLine("Пионерская"),
            Station.makeBlueLine("Филёвский парк"),
            Station.makeBlueLine("Багратионовская"),
            Station.makeBlueLine("Фили"),
            Station.makeBlueLine("Студенческая"),
            Station.makeBlueLine("Киевская"),
            Station.makeBlueLine("Смоленская"),
            Station.makeBlueLine("Арбатская"),
            Station.makeBlueLine("Александровский сад")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        topStations.forEach {
            val cp = MetroStationChip(this, it.name, it.color, MetroStationChip.TOP_SCAFFOLD)
            cp.setOnClickListener(this::onScaffoldItemClicked)
            top.addView(cp)
        }

        bottomStations.forEach {
            val cp = MetroStationChip(this, it.name, it.color, MetroStationChip.BOTTOM_SCAFFOLD)
            cp.setOnClickListener(this::onScaffoldItemClicked)
            bottom.addView(cp)
        }

    }

    private fun onScaffoldItemClicked(v: View) {
        if (!(v is MetroStationChip)) return
        when (v.scaffoldId) {
            MetroStationChip.TOP_SCAFFOLD -> {
                top.removeView(v)
                v.scaffoldId = MetroStationChip.BOTTOM_SCAFFOLD
                bottom.addView(v)
            }

            MetroStationChip.BOTTOM_SCAFFOLD -> {
                bottom.removeView(v)
                v.scaffoldId = MetroStationChip.TOP_SCAFFOLD
                top.addView(v)
            }
        }
    }
}
