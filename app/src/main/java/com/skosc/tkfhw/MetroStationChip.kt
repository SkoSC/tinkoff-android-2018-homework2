package com.skosc.tkfhw

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.google.android.material.chip.Chip


@SuppressLint("ViewConstructor")
class MetroStationChip(ctx: Context, private val name: String, @ColorRes color: Int, var scaffoldId: Int) : Chip(ctx) {
    companion object {
        const val TOP_SCAFFOLD = 0
        const val BOTTOM_SCAFFOLD = 1
    }

    init {
        text = name
        val drawable = ContextCompat.getDrawable(context, R.drawable.circle)!!
        val colorInt = ContextCompat.getColor(context, color)
        drawable.setColorFilter(colorInt, PorterDuff.Mode.MULTIPLY)
        chipIcon = drawable
    }

    override fun toString(): String = name

}